Pattern Matching
-
Fox has [pattern matching](https://en.wikipedia.org/wiki/Pattern_matching), a technique which is really useful to control the flow of your program based on kind of matching over a variable or expression.


For example, a command system using pattern matching over an array of strings may look like this:
```c++
import println, input from std.io.console

loop {
  if ( Some( cmd ) = input( ">>> " ) )
    when ( cmd.split( " " ) ) {
      [ "greet", name ] -> { println( "Hi ${name}! nice to meet you!" ) }
      [ "set", key, "to", value ] -> println( "set config key '${key}' to '${value}'" )
      [ "quit", code = "0" ] -> exit( code.parse<u8>() )
      ...
      it -> println( "Unknown command: ${it[0]}" )
    }
}
```

Now, this can at first look seem pretty complex, but it isn't really; lets go through it line by line.

```c++
loop {
```
In this line, we declare a loop, which will run the statement after it ( a block in this case ) until stopped.

```fox
if ( Some( cmd ) = input( ">>> " ) )
```
Here, we first ask for some input from the user, and then check if we were given a string, if not, we skip the next statement.

```fox
when ( cmd.split( " " ) ) {
```
This is where the fun begins, we split the input `String` on spaces, and then match on the formed array.

```fox
[ "greet", name ] -> { println( "Hi ${name}! nice to meet you!" ) }
```
This is doing matching on an array, checking if the fist element is `"greet"`, and then setting the `name` variable to the second element of the array,
now, this will _ONLY_ match, and execute the block on the right, if the array we're matching has 2 items and the first happen to be the string `"greet"`.

```fox
[ "set", key, "to", value ] -> println( "set config key '${key}' to '${value}'" )
```
Nothing much is happening, same principles as above apply.

```fox
[ "quit", code = "0" ] -> exit( code.parse<u8>() )
```
Now this is different! here, we have an _optional_ element in the pattern!
The first part, the `"quit"` is like we've already seen, but the second is a bit different, it has a _default value_, which will be used in case the matched array is only 1 element long.

```fox
it -> println( "Unknown command: ${it[0]}" )
```
While here, we just set `it` to the entire array, giving a way to make an error message. 
