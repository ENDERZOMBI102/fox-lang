Functions
-

Functions are one of the most basic types of control flow and code reuse units that we have access to.   

### Classic functions
Functions are defined a bit like variables, they start with an identifier, then parameters, return type and the body:
```fox
foo( bar: u8 ) u16 {
  return bar + 1
}
```
This function, named `foo`, takes in a single parameter of name.   

Now let's take a look at the simplest function, one that takes no arguments and returns nothing:
```fox
foo() { }
```
That looks quite shorter, We can see that if a function is `void`, the return type can be omitted, as it is implicit.

What other shenanigans can we do with the return type? We can set it to `auto` to derive it from the `return` statements:
```fox
foo( bar: u8 ) auto {
    return bar + 1
}
```
Here, the return type is detected as `u16` thanks to integer promotion rules.

### Single-expression functions
With the basics of functions laid off, let's get into another type of function syntax: single-expression functions.
Single-expression functions are a way to write less code to do the same thing: return the result of a single expression:
```fox
foo( bar: u8 ) auto =
    bar + 1
```
Here we can see the last example remade with a single-expression function.
