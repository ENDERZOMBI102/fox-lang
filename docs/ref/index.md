### Welcome to the Fox language reference

Let us start with a snippet of mildly complex Fox code:
```rust
import println from std.io.console

fun typeNameOf<T>( obj: T ) =
    T.nameof

pub fun main( argv: String[] ) i32 {
    config: &mut HashMap<String, String>( std::alloc::StackAllocator.instance() )

    loop {
        if ( Some( input ) != std::io::console::input( ">>> " ) )
            break

        when ( input.split( " " ) ) {
            [ "pi" ] -> {
                pi = 3.14159265358979323846f64
                println( "pi's type is ${pi.typeNameOf()}" )
            }
            [ "config", "set", key, value.. ] -> {
                config[ key ] = value.join( ' ' )
                continue
            }
            [ "config", "print" ] -> {
                for ( key, value in &config )
                    print( "- `$key` is `$value`" )
            }
            _ -> println( "Unknown command: $input" )
        }
    }
    println( "Goodbye!" )
}
```
by the end of reading the reference, you should be able to recognize and understand what each part does, if you're not already able to.


Table of contents:
- [Functions](functions/functions.md)
- [Statements](statements.md)
- [Pattern Matching](pattern_matching.md)


