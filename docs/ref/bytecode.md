
## Identifiers

- (0x00) `.import <path: String>` - imports the following identifier and all its members, if applicable

- (0x01) `.global <name>: <type>` - declares a global variable with type

- (0x02) `.local <name>: <type>` - declares a local variable with a type (only available in Structs and Classes)

- (0x03) `.init` - function body for variable initializer

- (0x04) `.struct <name>` - declares a struct

- (0x05) `.class <name>; <inherit>; <interface>` - declares a class that inherits other classes (multiple inheritance allowed, unless inheriting a foreign class) and implements interfaces

- (0x06) `.func <name>; <param>: <type>; <returntype>` - declares a function with the defined signature
- (0x07) `.end` - ends a body


## Opcodes

Opcodes are split into multiple subsections, each making it increasingly more explicit.
An opcode will be at most 2 bytes, not including parameters

### Opcode structure:
```
Variable ops (first byte starts with 00)
Byte 1: scope + stack op
	first 4 bits: scope 
		0000 : stack - Pushes/Pops from the stack
		0001 : param - Loads/Stores function parameters/function variables
		0010 : local - Loads/Stores local variables
		0011 : global - Loads/Stores global variables
	second 4 bits: stack op
		0000 : load - Pushes the stored value to the stack
		0001 : store - Pops the stack to the stored value
		TBD
Byte 2: Type
	00000000: class/struct
	00000001: bool
	00000010: i32
	00000011: f32
	00000100: i64
	00000101: f64
	00000110: char
	00000111: any
	00001000: void
	10000000: class/struct array
	10000001: bool array
	10000010: i32 array
	10000011: f32 array
	10000100: i64 array
	10000101: f64 array
	10000110: char array
	10000111: any array
	10001000: void array
	TBD

```

## Example
```
.import "std"
	.class String
.import "std/io"
	.func print; a: any; void

.global owo: cl/String
	.init
			stack.init.cl String [char [hewwo~] #initializes a String with a char array of `hewwo~`
			ret #returns the value
	.end

.func main; args: [cl/String; void
	global.load.cl String owo #load owo onto stack
	call print;any;void
	pop
	ret
.end
	
```
