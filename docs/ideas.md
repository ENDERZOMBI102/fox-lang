## Fox ideas
Ideas on which Fox is based upon
> NOTE: Stuff marked with `(?)` is TBD!
- Compiled but with compile time evaluation
- Native C/C++/JVM/CPython interop ( aka `export "$ID" ({}|DEF)` )
- Ability to manually use ASM in blocks ( `asm {}` )
- Comments ( `/* block comment /* nested block comment */ */` `// line comment` )
- Strongly typed
- Statically typed
- Structural typing via templates
- Functions ( `fun hello() [*:0]u8 = "Hello World!"` )
  - May infer return type ( `fun hello() auto = "Hello World!"` )
  - May be inline ( `inline fun hello() auto = "Hello World!"` )
  - Variadic arguments ( `fun thing( it: String... ) void` in which `it` will be a slice of `String` values )
  - Deferred methods ( `fun toString() String by id` defer the impl of a method to a field of the class )
- Unified Function Call Syntax ( `fun double( it: i32 ): i32 = it * 2` may be called as `32.double()` )
- Lambdas ( `{ param: i64 -> return param.toString() }` )
  - Callable type ( `() -> void` is a callable with 0 args and that returns void )
  - Callable with receiver ( `String.() -> void` is a callable with 1 arg that returns void, that if used with a lambda the lambda will have a hidden `this` arg set )
- Operator overloading ( `operator fun plus( this, other: i32 ) = this.toInt() + other` )
- Interfaces ( `interface ToString { fun toString( this ): String }` )
- Template-based Generics ( `fun plus<T>( value: T ) -> value` )
- Class (de)constructors ( `[~]classname()` )
- Arrays ( `a: i32[4] = { 0, 1, 2, 3 }` )
- Slices ( `a: i32[] = { 0, 1, 2, 3 }` )
- Different types of Pointers
  - Single item ( `a: *i32 = &value` )
  - Multiple items ( `a: [*]i32 = vector.data.cpy()` )
  - Multiple items, ended by value ( `a: [*:0]i32 = vector.data.cpy()` )
- References ( `a: &i32 = someVar` )
- Casting
  - Type ( `y as i64` )
  - Overload-able ( `y dynamic as i64` )
  - Byte reinterpretation ( `y reinterpret as i64` )
- Named literals ( `num"1234" == 1234` will call `operator literal num( value: String ) { }` )
  - Must be callable at compile-time
- `when` statement/expression
  - Pattern matching
  - Able to "match" on type, value, enum...
- Direct access to package members without import ( `std::io::console::print()` will reference the function directly, without imports, doesn't clash with variables )
- Byte-alike structs ( `struct Color like u32 { red: u8, green: u8, blue: u8, alpha: u8 }` which is also a `u32` )
- Classes are reference types, while structs are value types and can use the `like` keyword ( this means that classes will default to refs, while structs to copy/move depending on if they are primitive-like or not )
- Visibility modifiers ( `pub` ... )
- Macros (?)
  - More powerful than just search-replace ( `macro not ( value ) { ( value as bool ) != true }` may be used as `not!true` )
  - Typed variant ( `macro<T, R> ( value: T ) apply ( body: T.() -> R ) { body.call( value ) }` may be used as `12 apply! { -> this - 12 }` ) (?)
- Rich Standard library
  - may be dynamically or statically linked to, or not at all ( useful on bare metal )
  - should be able to be shrunk to only ship what one needs, ( per-use linking ) (?)
- Compiler will be first implemented in rust, and later when basic structure is done, rewritten in fox itself

## Examples
### Hello world
    from std.io.console import print 

    fun main( argv: String[] ) {
        print!( "Hello World!" )
    }

### Struct and UFCS
    struct Fox { } // Empty struct
    
    fun scream( fox: Fox ) { // May be called with `fox.scream()`
        //...
    }

### If expression
    from std.util.rnd import boolean
    from std import io
    
    fun getStream() io::Stream {   // T is required to have a `print()` method that takes in a `String` and returns `void`
        std = boolean()   // imported reference, type is inferred
        return if ( std )
            io::out       // direct reference via package member access
        else
            std::io::err  // doesn't clash with variable with same name
    }

### When, yield and UFCS
    from std.abs import Number // all number-y primitives "implement" this
    from std.util import UUID
    from std import String

    struct Record {
        name: String, uuid: UUID, value: &Number
        
        const NULL = Record { "", UUID.NULL, &0 }
    }

    // function that checks that the field `name` of a Record is equal to the string `Richard`
    fun isRichard( this: Record ) bool = this.name == "Richard"
    
    fun isNice( record: Record ) bool = when(record) {
            // checks if the record's `value` field is a `i32` and if it is the value `69`
            .value is i32 && .value == 69 -> true
            // checks if the record is the constant `Record.NULL` or if the name is empty
            Record.NULL || .name.empty() -> false
            // calls the `isRichard` function via UFCS
            .isRichard() -> {
                // yields the result of the expr on the right from the block
                yield ( record.uuid as i16[] ).sum() == 69420
            }
            // nothing worked, thats not nice
            else -> false
        }

### Compile-time-evaluated initialization blocks
    from std.io.fs import File
    
    const readme: String = comptime {
        yield File("./README.md").readString() ?: "N/D"
    }

### Visibility modifiers
    pub       fun  zero() { } // visible to everyone
    pub(den)  fun   one() { } // visible to a target ( like rust's `pub(crate)` )
    pub(pkg)  fun   two() { } // visible to all the compilation units in the current munit's package ( like java's `package private` )
    pub(this) fun three() { } // visible only to the current scope
              fun  four() { } // visible only to the current class and its subclasses
