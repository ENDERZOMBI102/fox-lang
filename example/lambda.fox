// Created by ENDERZOMBI102 on 13/03/2023.

// lambdas without return
typealias Callback<T> = fun(T) void

struct Context { /* ... */ }
fun say( this: *mut Context, message: []u8 ) { /* ... */ }

fun function( ctx: *mut Context ) {
	ctx.say( "hello world!" )
}
pub fun main() {
	var callback: Callback<*mut Context>

	callback = { ctx: *mut Context -> // explicit param type
		ctx.say( "Hello world!" )
	}

	callback = { ctx -> // param type inferred by usage
		ctx.say( "Hello world!" )
	}

	callback = function // first-class functions


	ctx = Context()
	callback.invoke( ctx ) // call the function, using the "method" syntax
	callback( ctx )        // same as above, using the "call" syntax
	ctx.callback()         // same as above, using the "UFCS" syntax
}


// lambdas with return
typealias Binary<T, R> = fun(T, T) R

fun main() {
	var binary = { a, b ->  a + b } // error: couldn't infer generic types `T` and `R` of typealias `Binary`
}
fun main() {
	var binary = { a, b -> return a + b } // type inferred by next call
	var value: u64 = binary( 20u32, 20u32 )
	var thing: []u8 = binary( "hello ", "world" ) // error: `binary(u32,u32): u64` is not compatible with type `[]u8]` // or better error idk
}
fun main() {
	var binary = { a, b -> return a + b } // type inferred by next call
	var value = binary( 20u32, 20u32 ) // value's inferred type is `u64`, as `u32 + u32` may be bigger than `u32`
}


// lambdas as parameter
typealias Processor<T> = fun([]u8) T

fun apply<T>( this: []u8, processor: Processor<T> ) T {
	return processor( this )
}
fun main() {
	"Hello world!".apply( []u8.upper )                           // "HELLO WORLD"
	"Hello World!".apply({ string -> return string.lower() })    // "hello world"
	"Hello world!".apply { string -> return string.split(" ") }  // [ "Hello", "world!" ]
}
