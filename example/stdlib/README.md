stdlib
-
An example implementation of the fox's standard library.

This is used as a way to test the syntax and features of the language, as such, it may contain files that do not fit for the stdlib, not meant for use.
