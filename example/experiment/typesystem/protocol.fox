// Created by ENDERZOMBI102 on 05/01/2023.
// NOTE: THIS IS JUST A THOUGHT AND SYNTAX EXPERIMENT FOR _HOW_ A POTENTIAL FEATURE MIGHT LOOK LIKE IF ADDED
from std import String
from std.io.console import print

/**
 * Protocols are basically a way to use "duck typing" in the language.
 * Duck typing can be summed up in this sentence: "if it walks like a duck and quack like a duck, then it is a duck".
 */
pub protocol At {
	fun at( this: *Self, other: *At )
}

pub class Foo {
	pub fun at<T : At>( this: *Self, other: T ) { //
		print( "T at Foo is {}", .{ other.typeof.name } )
	}
	pub fun toString( this: *Self ) String =
		"Foo"
}

pub class Bar {
	pub fun at<T : At>( this: *Self, other: T ) {
		print( "T at Bar is {}", .{ other.typeof.name } )
	}
	pub fun toString( this: *Self ) String =
		"Bar"
}

pub fun main() {
	var foo = Foo()
	var bar = Bar()

	foo.at( foo )  // stdout: T at Foo is Foo
	foo.at( bar )  // stdout: T at Foo is Bar
	bar.at( bar )  // stdout: T at Bar is Bar
	bar.at( foo as ToString ) // compiler error: ToString has no member at(AtToString)
}
