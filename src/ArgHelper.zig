const std = @import("std");
const Self = @This();

executable: [:0]const u8,
allocator: std.mem.Allocator,
args: std.ArrayList([:0]const u8),

pub fn init( allocator: std.mem.Allocator, argv: [][:0]const u8 ) Self {
	return .{
		.allocator = allocator,
		.executable = argv[0],
		.args = std
			.ArrayList([:0]const u8)
			.fromOwnedSlice( allocator, argv[1..argv.len] )
	};
}

pub fn fromSystem( allocator: std.mem.Allocator ) !Self {
	const argv = try std.process.argsAlloc( allocator );
	return init( allocator, argv );
}

pub fn deinit( self: *Self ) void {
	self.args.deinit();
	self.* = undefined;
}

pub fn flag( self: *Self, name: []const u8 ) bool {
	for ( self.args.items, 0.. ) |arg, i| {
		if ( std.mem.eql(u8, arg, name) ) {
			_ = self.args.orderedRemove( i );
			return true;
		}
	}
	return false;
}

pub fn flagMany( self: *Self, name: []const u8 ) usize {
	var res: usize = 0;
	for ( self.args.items, 0.. ) | arg, i | {
		if ( std.mem.eql(u8, arg, name) ) {
			_ = self.args.orderedRemove( i );
			res += 1;
		}
	}
	return res;
}

pub fn option( self: *Self, name: []const u8 ) !?[:0]const u8 {
	for ( self.args.items, 0.. ) | arg, i | {
		if ( std.mem.eql(u8, arg, name) ) {
			// did the option not get a value?
			if ( i + 1 >= self.args.items.len or self.args.items[i + 1][0] == '-' ) {
				_ = try std.io.getStdErr()
					.writer()
					.print( "error: Missing value for argument `{s}`\n", .{ name } );
				std.process.exit( 1 );
			}

			_ = self.args.orderedRemove( i );
			return self.args.orderedRemove( i );
		}
	}
	return null;
}

pub fn optionMany( self: *Self, name: []const u8 ) !std.ArrayList([:0]const u8) {
	var res = std.ArrayList([:0]const u8).init( self.allocator );
	while ( try self.option( name ) ) |it| {
		try res.append( it );
	}
	return res;
}

pub fn positional( self: *Self ) ?[:0]const u8 {
	// guard against empty array
	if ( self.args.items.len  == 0 )
		return null;

	return self.args.orderedRemove( 0 );
}

pub inline fn remaining( self: *Self ) std.ArrayList([:0]const u8) {
	// everything else
	return self.args;
}

test "argument helper" {
	const argv: [][:0]u8 = .{ "/tmp/argparse", "i-command-u-to", "--say", "hi", "-v", "1", "-v", "1", "-v", "1", "-v", "1", "--do", "nothing", "--do", "something", "--do", "everything" };

	const helper = Self.init( std.testing.allocator, argv );
	std.testing.expect( helper.flag("--hello") == false );
	std.testing.expect( helper.flagMany("-v") == 4 );
	std.testing.expectEqualStrings( "hi", helper.option("--say") orelse "nothing" );
	std.testing.expectEqualDeep( .{}, (try helper.optionMany("--do")).items );
	std.testing.expectEqualStrings( "/tmp/argparse", helper.executable );
	std.testing.expectEqualStrings( "i-command-u-to", helper.positional() orelse "nothing" );
	std.testing.expectEqualDeep( .{}, helper.remaining().items );
}
