const std = @import("std");
const CompilationContext = @import("compiler/CompilationContext.zig");
const Token = @import("compiler/Token.zig");
const TokenTree = @import("compiler/TokenTree.zig").TokenTree;
const Lexer = @import("compiler/Lexer.zig");
const Parser = @import("compiler/Parser.zig");

const stdout = std.io.getStdOut().writer();
const stderr = std.io.getStdErr().writer();

pub const Emissions = struct {
	tokens: bool = false,
	tokenTree: bool = false,
	tokenFox: bool = false,
	ast: bool = false,
};

/// Compiles the given files and emits the requested type of outputs.
///
/// Compilation of a file consists of various stages:
/// - File is loaded into memory, and then converted to UTF-8 if necessary.
/// - Lexer performs tokenization.
/// - Macros are separated from the rest of the code, as they need special handling.
/// - AST parsing
/// - Semantic analisys
///   -
///   -
///   -
/// - IR emission
/// - ....
///
/// These steps are ofc subject to change; but look good _in theory_.
///
/// * `emissions`: list of types of output for the compiler to emit.
/// * `files`: files to compile.
pub fn run( emissions: Emissions, files: []const []const u8, allocator: std.mem.Allocator ) !void {
	for ( files ) |path| {
		// init compilation
		_ = try stdout.print( "compiling: {s}\n", .{ path } );
		var ctx = CompilationContext.init( allocator, path );

		// read file contents
		const file = std.fs.openFileAbsolute( path, .{} ) catch |err| {
			switch (err) {
				error.FileNotFound => try stderr.print( "the given path `{s}` does not point to a valid file.\n", .{ path } ),
				else => unreachable,
			}
			std.process.exit(1);
		};

		const size = (try file.metadata()).size();
		const buf: []u8 = try ctx.allocator.alloc( u8, size );

		const read = file.read( buf ) catch unreachable;
		std.debug.assert( read == size );

		// lex
		_ = try stdout.print( "  Tokenizing..\n", .{} );
		var lexer = Lexer { };
		const tokens = try lexer.lex( &ctx, buf );
		if ( emissions.tokens ) {
			const handle = try ctx.createOutputFile( "toks" );
			defer handle.close();
			_ = try handle.writer().print( "{any}", .{ tokens.items } );
		}

		// parse
		_ = try stdout.print( "  Parsing structure..\n", .{} );
		const tree = try Parser.parse( &ctx, tokens.items );
		if ( emissions.tokenTree ) {
			const handle = try ctx.createOutputFile( "ast" );
			defer handle.close();
			_ = try handle.writer().print( "{}", .{ tree } );
		}
	}
}
