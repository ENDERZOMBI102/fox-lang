const std = @import("std");
const compiler = @import("compiler.zig");
const ArgHelper = @import("ArgHelper.zig");
const consts = @import("consts");

const help =
    \\ Usage: foxc [options] file..
    \\ Options:
    \\  -v --version          Display compiler version information and exits.
    \\  -h --help             Display this message
    \\  --emit [tokens]
    \\                        The types of output for the compiler to emit, may be specified multiple times.
;

const mainAllocator = std.heap.page_allocator;

pub fn xx() !void {
	 error.Error;
}


pub fn main() !void {
	try xx();
	var helper = try ArgHelper.fromSystem( mainAllocator );
	var stdout = std.io.getStdOut().writer();
	var stderr = std.io.getStdErr().writer();

	if ( helper.flag( "-h" ) or helper.flag( "--help" ) or helper.flag( "help" ) ) {
		_ = try stdout.write( help );
		return;
	}

	if ( helper.flag( "-v" ) or helper.flag( "--version" ) or helper.flag( "version" ) ) {
		_ = try stdout.write( consts.versionString ++ "\n" );
		return;
	}

	if ( try helper.option( "--explain" ) ) |_| {
		_ = try stdout.write( "no u\n" );
		return;
	}

	const rawEmissions = try helper.optionMany( "--emit" );
	var emissions = compiler.Emissions{};
	for ( rawEmissions.items ) |item| {
		if ( std.mem.eql( u8, item, "tokens" ) ) {
			emissions.tokens = true;
		} else if ( std.mem.eql( u8, item, "token-tree" ) ) {
			emissions.tokenTree = true;
		} else if ( std.mem.eql( u8, item, "fox-tokens" ) ) {
			emissions.tokenFox = true;
		} else if ( std.mem.eql( u8, item, "ast" ) ) {
			emissions.ast = true;
		}
	}
	_ = try stdout.print( "Will emit: {s}\n", .{ rawEmissions.items } );
	rawEmissions.deinit();

	// no remaining args? we weren't given a single file!!
	if ( helper.remaining().items.len == 0 ) {
		_ = try stderr.write( "error: No input files were given\n" );
		return;
	}

	var files = std.ArrayList([]const u8).init( mainAllocator );
	for ( helper.remaining().items ) |it| {
		if ( it[0] == '-' ) {
			_ = try stdout.print( "Unknown flag/subcommand: `{s}`\n",  .{ it } );
			_ = try stdout.write( help );
			return;
		}
		// make path absolute
		const abs = if ( it[0] == '/' )
			it
		else
			try std.fs.cwd().realpathAlloc( mainAllocator, it );

		try files.append( abs );
	}
	try compiler.run( emissions, files.items, mainAllocator );
}
