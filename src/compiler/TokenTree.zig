const std = @import("std");
const CompilationContext = @import("CompilationContext.zig");

pub fn TokenTree( comptime TokenType: type ) type {
	return struct {
		const Self = @This();

		const Child = union(enum) {
			Token: *TokenType,
			Tree: Self,

			pub fn format( self: @This(), comptime _: []const u8, _: std.fmt.FormatOptions, writer: anytype ) !void {
				switch ( self ) {
					.Token => |*value| try writer.print( "Token: {s}", .{ value.* } ),
					.Tree => |*value| try writer.print( "Tree: {s}", .{ value } )
				}
			}
		};

		/// The token delimiters, `null` if root
		delimiters: ?struct { pre: []const u8, suf: []const u8 },
		/// The range of the contained tokens
		range: struct { start: usize, end: usize },
		/// Contained tokens and trees
		children: std.ArrayList(Child),

		pub fn format( self: Self, comptime _: []const u8, _: std.fmt.FormatOptions, writer: anytype ) !void {
			if ( self.delimiters ) |delim| {
				try writer.print( "TokenTree(pre={s},suf={s}", .{ delim.pre, delim.suf } );
			} else {
				try writer.print( "TokenTree(pre=_,suf=_", .{} );
			}

			try writer.print( ",range={}..{},children={s})", .{ self.range.start, self.range.end, self.children.items } );
		}


		pub fn parse( ctx: *CompilationContext, tokens: []TokenType ) !Self {
			var arena = try ctx.newArena( "TokenTree" );
			errdefer arena.deinit();
			const allocator = arena.allocator();

			const Stack = std.SinglyLinkedList(Self);
			var stack = Stack { };

			stack.prepend( blk: {
				const node = try allocator.create( Stack.Node );
				node.data = .{
					.delimiters = null,
					.range = .{ .start = 0, .end = tokens.len - 1 },
					.children = std.ArrayList(Child).init( ctx.allocator ),
				};
				break :blk node;
			});

			for ( tokens, 0.. ) |*token, i| {
				inline for ( [4] *const [1:0]u8{ "{", "[", "(", "<" } ) |start| {
					if ( std.mem.eql( u8, start, token.value ) ) {
						const node = try allocator.create( Stack.Node );
						node.data = .{
							.delimiters = .{ .pre = token.value, .suf = switch ( token.value[0] ) { '{' => "}", '[' => "]", '(' => ")", '<' => ">", else => unreachable } },
							.range = .{ .start = i, .end = undefined },
							.children = std.ArrayList(Child).init( ctx.allocator ),
						};
						stack.prepend( node );
					}
				}
				inline for ( [4] *const [1:0]u8{ "}", "]", ")", ">" } ) |end| {
					if ( std.mem.eql( u8, end, token.value ) ) {
						try stack.first.?.data.children.append(.{ .Token = token });
						var tree = stack.popFirst().?.data;
						tree.range.end = i;
						try stack.first.?.data.children.append(.{ .Tree = tree });
					}
				}

				try stack.first.?.data.children.append(.{ .Token = token });
			}

			return stack.first.?.data;
		}

		const TokenList = std.ArrayList(TokenType);
		pub fn flatten( self: *const Self, alloc: std.mem.Allocator ) !TokenList {
			var tokens = TokenList.init(alloc);

			for ( self.children.items ) |child| {
				switch ( child ) {
					.Token => |it| tokens.append( it.* ),
					.Tree => |it| it.flattenTo( *tokens ),
				}
			}

			return tokens;
		}

		pub fn flattenTo( self: *const Self, list: *TokenList ) void {
			for ( self.children.items ) |child| {
				switch ( child ) {
					.Token => |it| list.append( it.* ),
					.Tree => |it| it.flattenTo( list ),
				}
			}
		}
	};
}

// test "flatten creates same token stream" {
// 	const SimpleToken = @import("SimpleToken.zig");
// 	var ctx = CompilationContext.init( std.testing.allocator, "<test>" );
// 	const toks = SimpleToken.lex( &ctx, "fun main() i32 {\nreturn 0\n}\n" ) catch unreachable;
// 	const tree = TokenTree(SimpleToken).parse( &ctx , toks.items ) catch unreachable;
// 	try std.testing.expectEqualSlices( SimpleToken, toks.items, try tree.flatten( std.testing.allocator ) );
// }

