const std = @import("std");
const Self = @This();

/// The current memory allocator.
allocator: std.mem.Allocator,
/// The absolute path of the file which is being processed.
path: []const u8,
arenas: std.StringHashMap(std.heap.ArenaAllocator),


pub fn init( allocator: std.mem.Allocator, path: []const u8 ) Self {
	return Self{
		.allocator=allocator,
		.path=path,
		.arenas = std.StringHashMap(std.heap.ArenaAllocator).init( allocator )
	};
}

/// Creates an output file for this context and returns an open handle to it.
///
/// NOTE: Ideally, the path should be something like `$project$/.fox/out/$path/${name}.$kind`.
/// * `kind`: The "kind of output", (may be same as extension).
pub fn createOutputFile( self: *Self, kind: []const u8 ) !std.fs.File {
	const abs = try std.mem.join( self.allocator, ".", &.{ self.path, kind } );

	return try std.fs.createFileAbsolute( abs, .{ } );
}

/// Creates an Arena Allocator for use, which is also memory-tracked.
///
/// * `name`: The name of the phase that is requesting the arena.
pub fn newArena( self: *Self, name: []const u8 ) !*std.heap.ArenaAllocator {
	if ( self.arenas.getPtr( name ) ) |arena|
		return arena;

	try self.arenas.put( name, std.heap.ArenaAllocator.init( self.allocator ) );
	return self.arenas.getPtr( name ).?;
}


