const std = @import("std");
const Token = @import("Token.zig");
const Self = @This();

pub const TokenIndex = u32;
pub const TokenList = std.MultiArrayList(Token);
pub const NodeList = std.MultiArrayList(Node);


tokens: TokenList,
nodes: NodeList,
imports: std.ArrayList(Node.Index),
decls: std.ArrayList(Node.Index),
root: ?Node.Index,

// TODO: These
pub fn init() Self { }
pub fn deinit() void { }

pub const Node = struct {
	tag: Tag,
	/// Starting token of the node
	start: TokenIndex,
	/// Last token of the node
	end: TokenIndex,
	/// Documentation comment
	docs: ?TokenIndex = null,

	/// Data related to this node
	component: Component,

	pub const Index = u32;

	pub const Tag = enum(u8) {
		Import,
		FunctionDecl,
		Call,
		Identifier
	};

	pub const Component = union(enum) {
		Nothing,
	};
};
