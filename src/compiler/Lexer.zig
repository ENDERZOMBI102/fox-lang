const std = @import("std");
const CompilationContext = @import("CompilationContext.zig");
const Token = @import("Token.zig");
const Self = @This();

const binDigits = "01";
const octDigits = "01234567";
const decDigits = "0123456789";
const hexDigits = "0123456789ABCDEFabcdef";
const softKeywords: []const []const u8 = &.{
	"any", "attribute", "by", "class", "den", "dynamic",
	"false", "from", "import", "like", "macro",
	"memory", "module", "package", "Self", "static",
	"this", "true", "type", "undefined",
};
const keywords: []const []const u8 = &.{
	"abstract", "and", "as", "asm", "break", "comptime", "const", "continue",
	"defer", "do", "else", "enum", "export", "extern", "for", "fun", "if", "in",
	"interface", "is", "loop", "mut", "not", "operator", "or", "override", "pub",
	"reinterpret", "return", "sealed", "struct", "typealias", "unreachable", "var",
	"when", "while", "xor", "yield",
};

tokens: std.ArrayList(Token),
i: usize = 0,
col: u16 = 1,
line: u16 = 1,
text: []const u8,

pub fn init( ctx: *CompilationContext, text: []const u8 ) Self {
	return .{
		.tokens = std.ArrayList(Token).init( ctx.allocator ),
		.text = text
	};
}

fn push( self: *Self, tag: Token.Tag, start: usize, end: usize ) !void {
	try self.tokens.append(.{
		.tag = tag,
		.pos = .{ .line=self.line, .col=self.col },
		.range = .{ .start = start, .end = end },
		.value = self.text[start .. end + 1],
	});
}

pub fn lex( self: *Self ) !std.ArrayList(Token) {
	while ( self.i < self.text.len ) {
		const og = self.i;
		switch ( self.text[self.i] ) {
		// skip carriage return
			'\r' => self.i += 1,
			// line & column handling
			'\n' => {
				try self.push( .Newline, self.i, self.i );
				self.i += 1;
				self.line += 1;
				self.col = 1;
			},
			// whitespace skipping
			' ', '\t' => {
				const kind = self.text[self.i];
				while ( self.text.len > self.i and self.text[self.i] == kind ) {
					self.i += 1;
				}
				self.col += @intCast(self.i - og);
			},
			// numbers
			'0'...'9' => {
				// Is it a base N number?
				if ( self.text[self.i] == '0' and self.i + 2 < self.text.len and (self.text[self.i+1] == 'b' or self.text[self.i+1] == 'o' or self.text[self.i+1] == 'x') ) {
					// Determine the radix for the constant
					const radix: Token.Radix = switch ( self.text[self.i+1] ) {
						'b' => .Bin, 'o' => .Oct, 'x' => .Hex,
						else => unreachable
					};

					self.i += 2;
					while ( self.i < self.text.len and isDigit( self.text[ self.i ], radix ) ) {
						self.i += 1;
					}

					try self.push( .{ .Integer = radix }, og, self.i - 1 );
					self.col += @intCast(self.i - og);
					continue;
				}

				var kind: Token.Tag = .{ .Integer = .Dec };
				while ( self.i < self.text.len and isDigit( self.text[ self.i ], .Dec ) ) {
					self.i += 1;
				}
				if ( self.i < self.text.len and ('.' == self.text[self.i] or 'e' == self.text[self.i]) ) {
					kind = .Float;
					if ( self.text[self.i] == '.' ) {
						self.i += 1;
						while ( self.i < self.text.len and isDigit( self.text[self.i], .Dec ) ) {
							self.i += 1;
						}
					}
					if ( self.i < self.text.len and 'e' == self.text[self.i] ) {
						self.i += 1;
						if ( self.i < self.text.len and ('-' == self.text[self.i] or '+' == self.text[self.i]) ) {
							self.i += 1;
						}
						while ( self.i < self.text.len and isDigit( self.text[self.i], .Dec ) ) {
							self.i += 1;
						}
					}
				}

				try self.push( kind, og, self.i );
				self.col += @intCast(self.i - og);
				continue;
			},
			'{' => {
				try self.push( .SyLbrace, self.i, self.i );
				self.col += 1;
				self.i += 1;
				continue;
			},
			'}' => {
				try self.push( .SyRbrace, self.i, self.i );
				self.col += 1;
				self.i += 1;
				continue;
			},
			'[' => {
				try self.push( .SyLbrack, self.i, self.i );
				self.col += 1;
				self.i += 1;
				continue;
			},
			']' => {
				try self.push( .SyRbrack, self.i, self.i );
				self.col += 1;
				self.i += 1;
				continue;
			},
			'a' ... 'z', 'A' ... 'Z', '_', '$' => {
				while ( self.i + 1 < self.text.len and (std.ascii.isAlphanumeric( self.text[self.i] ) or self.text[self.i] == '_' or self.text[self.i] == '$') ) {
					self.i += 1;
				}

				if ( KEYWORDS.get( self.text[ og..self.i ] ) ) |tag| {
					try self.push( tag, og, self.i );
					self.col += @intCast(self.i - og);
					continue;
				}

				try self.push( .Identifier, og, self.i );
				self.col += @intCast(self.i - og);
				continue;
			},
			else => { }
		}
	}

	return self.tokens;
}

fn isDigit( x: u8, radix: Token.Radix ) bool {
	//  2 = 48-49              ~ x > 47 and (r !=  2 or x < 50)
	//  8 = 48-55              ~ x > 47 and (r !=  8 or x < 56)
	// 10 = 48-57              ~ x > 47 and (r != 10 or x < 58)
	// 16 = 48-57+65-70+97-102 ~ x > 47 and (r != 16 or (x < 58 or (x < 71 and x > 64) or (x < 103 and x > 96) ))

	return x > 47 // base case
		and (radix != .Bin or x < 50) // binary
		and (radix != .Oct or x < 56) // octal
		and (radix != .Dec or x < 58) // decimal
		and (radix != .Hex or (x < 58 or (x < 71 and x > 64) or (x < 103 and x > 96) )); // hexidecimal
}


const KEYWORDS = std.StaticStringMap(Token.Tag).initComptime(.{
	// hard
	.{ "abstract", .KwAbstract },
	.{ "and", .KwAnd },
	.{ "as", .KwAs },
	.{ "asm", .KwAsm },
	.{ "break", .KwBreak },
	.{ "comptime", .KwComptime },
	.{ "const", .KwConst },
	.{ "continue", .KwContinue },
	.{ "defer", .KwDefer },
	.{ "do", .KwDo },
	.{ "else", .KwElse },
	.{ "enum", .KwEnum },
	.{ "export", .KwExport },
	.{ "extern", .KwExtern },
	.{ "for", .KwFor },
	.{ "fun", .KwFun },
	.{ "if", .KwIf },
	.{ "in", .KwIn },
	.{ "interface", .KwInterface },
	.{ "is", .KwIs },
	.{ "loop", .KwLoop },
	.{ "mut", .KwMut },
	.{ "not", .KwNot },
	.{ "operator", .KwOperator },
	.{ "or", .KwOr },
	.{ "override", .KwOverride },
	.{ "pub", .KwPub },
	.{ "reinterpret", .KwReinterpret },
	.{ "return", .KwReturn },
	.{ "sealed", .KwSealed },
	.{ "struct", .KwStruct },
	.{ "typealias", .KwTypealias },
	.{ "unreachable", .KwUnreachable },
	.{ "var", .KwVar },
	.{ "when", .KwWhen },
	.{ "while", .KwWhile },
	.{ "xor", .KwXor },
	.{ "yield", .KwYield },
	// soft
	.{ "any", .SkwAny },
	.{ "attribute", .SkwAttribute },
	.{ "by", .SkwBy },
	.{ "class", .SkwClass },
	.{ "den", .SkwDen },
	.{ "dynamic", .SkwDynamic },
	.{ "false", .SkwFalse },
	.{ "from", .SkwFrom },
	.{ "import", .SkwImport },
	.{ "like", .SkwLike },
	.{ "macro", .SkwMacro },
	.{ "memory", .SkwMemory },
	.{ "module", .SkwModule },
	.{ "package", .SkwPackage },
	.{ "self", .SkwSelf },
	.{ "static", .SkwStatic },
	.{ "this", .SkwThis },
	.{ "true", .SkwTrue },
	.{ "type", .SkwType },
	.{ "undefined", .SkwUndefined },
});


// test "Numbers" {
// 	var ctx = CompilationContext.init( std.testing.allocator , "<test>" );
// 	var expected: [4]Self = undefined;
// 	// Integer/10 tokenizing
// 	expected[0] = .{
// 		.tag = .{ .Integer = .Dec },
// 		.pos = .{ .line=1, .col=1 },
// 		.range = .{ .start=0, .end=2 },
// 		.value = "12"
// 	};
// 	// Integer/2 tokenizing
// 	expected[1] = .{
// 		.tag = .{ .Integer = .Bin },
// 		.pos = .{ .line=1, .col=4 },
// 		.range = .{ .start=3, .end=9 },
// 		.value = "0b1100"
// 	};
// 	// Integer/8 tokenizing
// 	expected[2] = .{
// 		.tag = .{ .Integer = .Oct },
// 		.pos = .{ .line=1, .col=11 },
// 		.range = .{ .start=10, .end=14 },
// 		.value = "0o12"
// 	};
// 	// Integer/16 tokenizing
// 	expected[3] = .{
// 		.tag = .{ .Integer = .Hex },
// 		.pos = .{ .line=1, .col=16 },
// 		.range = .{ .start=15, .end=19 },
// 		.value = "0x12"
// 	};
// 	var actual = try lex( &ctx, "12 0b1100 0o12 0x12" );
// 	defer actual.deinit();
// 	try std.testing.expectEqualSlices( Self, &expected, actual.items );
// }
