const std = @import("std");
const CompilationContext = @import("CompilationContext.zig");
const Self = @This();

pub const Radix = enum { Hex, Oct, Bin, Dec };
pub const Tag = union(enum) {
	Identifier,
	Newline,
	// literals
	Integer: Radix,
	Float,
	String,
	LineString,
	// comments
	CoBlock,
	CoDocs,
	CoLine,
	// soft keywords
	SkwAny, SkwAttribute, SkwBy, SkwClass, SkwDen, SkwDynamic,
	SkwFalse, SkwFrom, SkwImport, SkwLike, SkwMacro,
	SkwMemory, SkwModule, SkwPackage, SkwSelf, SkwStatic,
	SkwThis, SkwTrue, SkwType, SkwUndefined,
	// keywords
	KwAbstract, KwAnd, KwAs, KwAsm, KwBreak, KwComptime, KwConst, KwContinue,
	KwDefer, KwDo, KwElse, KwEnum, KwExport, KwExtern, KwFor, KwFun, KwIf, KwIn,
	KwInterface, KwIs, KwLoop, KwMut, KwNot, KwOperator, KwOr, KwOverride, KwPub,
	KwReinterpret, KwReturn, KwSealed, KwStruct, KwTypealias, KwUnreachable, KwVar,
	KwWhen, KwWhile, KwXor, KwYield,
	// operator symbols
	SyInvert,            // ~
	SyAssign,            // =
	SyNotEq,             // !=
	SyEq,                // ==
	SyLess,              // <
	SyLessOrEqual,       // <=
	SyGreater,           // >
	SyGreaterOrEqual,    // >=
	SyCompare,           // <=>
	SyPlus,              // +
	SyPlusAssign,        // +=
	SyPlusWrap,          // +%
	SyPlusWrapAssign,    // +%=
	SyPlusSat,           // +|
	SyPlusSatAssign,     // +|=
	SyMinus,             // -
	SyMinusAssign,       // -=
	SyMinusWrap,         // -%
	SyMinusWrapAssign,   // -%=
	SyMinusSat,          // -|
	SyMinusSatAssign,    // -|=
	SyQuotient,          // /
	SyQuotientAssign,    // /=
	SyMul,               // *
	SyMulAssign,         // *=
	SyMulWrap,           // *%
	SyMulWrapAssign,     // *%=
	SyMulSat,            // *|
	SyMulSatAssign,      // *|=
	SyPow,               // **
	SyPowAssign,         // **=
	SyRemainder,         // %
	SyRemainderAssign,   // %=
	SyBitAnd,            // &
	SyBitAndAssign,      // &=
	SyBitXor,            // ^
	SyBitXorAssign,      // ^=
	SyBitOr,             // |
	SyBitOrAssign,       // |=
	SyShiftRight,        // >>
	SyShiftRightAssign,  // >>=
	SyShiftLeft,         // <<
	SyShiftLeftAssign,   // <<=
	SyShiftShiftRight,   // >>>
	SyRangeExclusive,    // ..
	SyRangeInclusive,    // ..=
	// other symbols
	SyLbrace,            // {
	SyRbrace,            // }
	SyLbrack,            // [
	SyRbrack,            // ]
	SyLparen,            // (
	SyRparen,            // )
	SyComma,             // ,
	SyDot,               // .
	SyColon,             // :
	SyColoncolon,        // ::
	SyDquote,            // "
	SyQuote,             // '
	SyArrowR,            // ->
	SyQuestion,          // ?
	SyEllipsis,          // ...
};

tag: Tag,
pos: struct { line: u16, col: u16 },
range: struct { start: usize, end: usize },
value: []const u8,

pub fn format( self: Self, comptime _: []const u8, _: std.fmt.FormatOptions, writer: anytype ) !void {
	var buf: [20]u8 = undefined;
	try writer.print(
		"Token(type={s}, line={}, col={}, range={}..{}, value=`{s}`)",
		.{
			switch ( self.tag ) {
				.Integer => |it| try std.fmt.bufPrint(&buf, "Integer({s})", .{ @tagName( it ) }),
				else => @tagName( self.tag )
			},
			self.pos.line, self.pos.col,
			self.range.start, self.range.end,
			switch ( self.value[0] ) {
				'\n' => "\\n",
				'\t' => "\\t",
				else => self.value
			}
		}
	);
}

pub fn ident( self: *Self ) bool {
	return switch ( self.tag ) {
		.SkwAny, .SkwAttribute, .SkwBy, .SkwClass, .SkwDen,
		.SkwDynamic, .SkwFalse, .SkwFrom, .SkwImport, .SkwLike,
		.SkwMacro, .SkwMemory, .SkwModule, .SkwPackage, .SkwSelf,
		.SkwStatic, .SkwThis, .SkwTrue, .SkwType, .SkwUndefined,
		.Identifier => true,
		else => false,
	};
}
