const std = @import("std");
const Ast = @import("Ast.zig");
const Token = @import("Token.zig");
const Lexer = @import("Lexer.zig");
const CompilationContext = @import("CompilationContext.zig");
const Self = @This();


ctx: *CompilationContext,
index: usize = 0,
tokens: []Token,


fn parseModule( self: *Self ) !Ast {
	const tokens = Ast.TokenList{};
	const nodes = Ast.NodeList{};
	const imports = std.ArrayList(Ast.Node.Index).init( self.ctx.allocator );
	const decls = std.ArrayList(Ast.Node.Index).init( self.ctx.allocator );
	const root = null;

	while ( self.index < self.tokens.len ) {

	}

	return .{
		.tokens = tokens,
		.nodes = nodes,
		.imports = imports,
		.decls = decls,
		.root = root,
	};
}

fn parseIndentifier( self: *Self ) ?Ast.Node {
	if ( self.tokens[self.index].ident() ) {
		defer self.index += 1;
		return .{
			.tag=.Identifier,
			.start=self.index,
			.end=self.index,
			.docs=null,
			.component=.Nothing
		};
	}

	return null;
}


pub fn parse( ctx: *CompilationContext, tokens: []Token ) !Ast {
	var parser = Self{ .ctx=ctx, .tokens=tokens };
	return parser.parseModule();
}

test "parser test" {
	var ctx = CompilationContext.init( std.testing.allocator , "<test>" );
	var lexer = Lexer.init( &ctx, "12 0b1100 0o12 0x12" );
	const toks = try lexer.lex();
	const actual = try parse( &ctx, toks.items );

	try std.io.getStdOut().writer().print( "{}\n", .{ actual } );
}

